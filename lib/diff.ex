defmodule Diff do

  def diff(s1, s2, debug \\ false) when is_binary(s1) and is_binary(s2) do
    {:ok, old_tokens} = Tokenizer.tokenize(s1)  
    {:ok, new_tokens} = Tokenizer.tokenize(s2)  
    diff_list = Tdiff.diff(old_tokens, new_tokens)
    {old, new} = List.foldr diff_list, {[], []}, &transform_diff/2
    # + O(N) complexity
    old_ = List.foldr old, [], &prettify/2 
    new_= List.foldr new, [], &prettify/2
    maybe_debug(debug, old_, new_)
    # json with iolist unexpected behaviour, convert to binary
    old_bin = :erlang.list_to_binary(old_)
    new_bin = :erlang.list_to_binary(new_)
    JSON.encode([old: old_bin, new: new_bin])
  end

  defp maybe_debug(false, _, _), do: :ok
  defp maybe_debug(true, old, new) do
    :file.write_file("./old.html", old)
    :file.write_file("./new.html", new)
  end

  defp transform_diff(e, acc) do
    handle_edit_type(e, acc)
  end
  
  #copying from left to right
  defp handle_edit_type({:eq, diff}, {old, new}), do:
    {diff ++ old, diff ++ new}
  defp handle_edit_type({:ins, diff}, {old, new}), do:
    handle_ins(diff, {old, new})
  defp handle_edit_type({:del, diff}, {old, new}), do:
    handle_del(diff, {old, new})

  defp handle_ins(["\n\n"], {old, new}), do:
    {old, green_style(["<br>&para;<br>"]) ++ new}
  defp handle_ins(["\r\n"], {old, new}), do:
    {old, green_style(["<br>&para;<br>"]) ++ new}
  defp handle_ins(diff, {old, new}), do:
    {old, green_style(diff) ++ new}

  defp handle_del(["\n\n"], {old, new}), do:
    {red_style(["<br>&para;<br>"]) ++ old, new}
  defp handle_del(["\r\n"], {old, new}), do:
    {red_style(["<br>&para;<br>"]) ++ old, new}
  defp handle_del(diff, {old, new}), do:
    {red_style(diff) ++ old, new}

  defp green_style(e) when is_list(e), do:
    ["<strong><span style='background-color: #84FF84;'>"] 
    ++ e ++ ["</span></strong>"]

  defp red_style(e) when is_list(e), do:
    ["<strong><span style='background-color: #FF3636;'>"] 
    ++ e ++ ["</span></strong>"]

  #TODO: possible improvement, prettify in previous stage, if speed would be not good enough
  defp prettify(e, acc) do
    case e do
      "\n\n" -> ["<br><br>"] ++ acc
      "\r\n" -> ["<br><br>"] ++ acc
      "\n" -> ["<br>"] ++ acc
      el -> [el] ++ [" "] ++ acc
    end
  end
  
end
