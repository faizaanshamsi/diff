defmodule Tokenizer do
  @doc "
    Tokenize input binary string into tokesn -- split binary with seprator
  "

  def tokenize(bin) when is_binary(bin), do:
    tokenize(bin, "", [])

  defp tokenize("", "", tokens), 
    do: {:ok, :lists.reverse(tokens)}
  defp tokenize("", word, tokens), 
    do: {:ok, :lists.reverse([word] ++ tokens)}
  defp tokenize(<<"\r\n", rest :: binary>>, word, tokens), do:
    tokenize(rest, "", ["\r\n"] ++ append_word(tokens, word))
  defp tokenize(<<"\n\n", rest :: binary>>, word, tokens), do:
    tokenize(rest, "", ["\n\n"] ++ append_word(tokens, word))
  defp tokenize(<<"\n", rest :: binary>>, word, tokens), do:
    tokenize(rest, "", ["\n"] ++ append_word(tokens, word))
  defp tokenize(<<" ", rest :: binary>>, word, tokens), do:
    tokenize(rest, "", [word] ++ tokens)
  defp tokenize(<<c :: size(8), rest :: binary>>, word, tokens), do:
    tokenize(rest, word <> <<c>>, tokens)

  defp append_word(tokens, ""), do: tokens
  defp append_word(tokens, word), do: [word] ++ tokens
    
end
