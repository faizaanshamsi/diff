# Diff

Сustomized version of diff function

## Usage

```
iex -S mix
iex(1)> Diff.diff("cat", "hat")
{:ok, json_string}
```

#### Files

```
{ok, old} = File.read("./old_.txt")
{ok, new} = File.read("./new_.txt")

Diff.diff(old, new, true) ## return json, and also create 2 html files
```

##### JSON format

```
{"old": "html_string", "new": "html_string"}
```
##### Debug

If you set 3rd param of diff function to *true*, if will create 2 files called "new.html" and "old.html" in current directory

```
iex -S mix
iex(1)> Diff.diff("cat", "hat", true)
{:ok, json_string}
```

Now check current dir for: "new.html" and "old.html"

## Installation

**Note: currently not available in Hex, add library like git dependency**

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `diff` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:diff, "~> 0.0.1"}]
    end
    ```

  2. Ensure `diff` is started before your application:

    ```elixir
    def application do
      [applications: [:diff]]
    end
    ```

